import './index.css';
import LoginForm from './components/LoginForm';
import Validation from './components/Validation';
import ReactHookForm from './components/ReactHookForm';

function App() {
  return (
    <div className="App">
     {/* <LoginForm/> */}
     {/* <Validation/> */}
     <ReactHookForm/>
    </div>
  );
}

export default App;
