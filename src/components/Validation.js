import React, { useState } from 'react';

const Validation = () => {
    const [type, setType] = useState("password");
    const [lowerValidate, setLowerValidate] = useState(false);
    const [upperValidate, setUpperValidate] = useState(false);
    const [numberValidate, setNumberValidate] = useState(false);
    const [specialValidate, setSpecialValidate] = useState(false);
    const [lengthValidate, setLengthValidate] = useState(false);

    const handleChange = (value) => {
        const lower = new RegExp('(?=.*[a-z])');
        const upper = new RegExp('(?=.*[A-Z])');
        const number = new RegExp('(?=.*[0-9])');
        const special = new RegExp('(?=.*[!@#$%^&*()_+])');
        const length = new RegExp('(?=.{8,})');

        // lowercase validation
        if (lower.test(value)) {
            setLowerValidate(true);
        } else {
            setLowerValidate(false);
        }
        if (upper.test(value)) {
            setUpperValidate(true);
        } else {
            setUpperValidate(false);
        }
        if (number.test(value)) {
            setNumberValidate(true);
        } else {
            setNumberValidate(false);
        }
        if (special.test(value)) {
            setSpecialValidate(true);
        } else {
            setSpecialValidate(false);
        }
        if (length.test(value)) {
            setLengthValidate(true);
        } else {
            setLengthValidate(false);
        }
    }

    const handleFormData =() =>{
        console.log()
    }

    return (
        <div className='text-center'>
            <form onSubmit={handleFormData}>
                <input type={type} className='border border-black' onChange={(e) => handleChange(e.target.value)} />
                {type === "password" ? <p className='text-xs' onClick={() => setType("text")}>show</p> : <p className='text-xs' onClick={() => setType("password")}> hide</p>}
                {
                    lowerValidate && upperValidate ? <input type="submit"  className='bg-emerald-400 cursor-pointer' /> : <input type="submit"  disabled className='bg-gray-500'/> 
                }
                
            </form>

            {/* validation tracker */}
            <div>
                <div className={lowerValidate ? "text-green-300" : "text-red-400"}>at least one lowercase letter</div>
                <div className={upperValidate ? "text-emerald-400" : "text-red-600"}>at least one uppercase letter</div>
                <div className={numberValidate ? "text-emerald-400" : "text-red-600"}>at least one number</div>
                <div className={specialValidate ? "text-emerald-400" : "text-red-600"}>at least one special character</div>
                <div className={lengthValidate ? "text-emerald-400" : "text-red-600"}>at least 8 characters</div>
            </div>
        </div>
    );
}

export default Validation;
