import React from "react";
import { useForm } from "react-hook-form";

const ReactHookForm = () => {
  const {
    register,
    handleSubmit,
    watch,
    formState: { errors },
  } = useForm();

  const handleFormSubmit = (data) => {
    console.log(data);
  };

  return (
    <div className="w-full h-full">
      <div className="max-w-lg mx-auto h-full py-4">
        <form
          className="flex justify-center items-center shadow-xl p-8 flex-col space-y-4 py-16"
          onSubmit={handleSubmit(handleFormSubmit)}
        >
          <div className="mb-3 w-full flex flex-col items-start justify-center gap-2">
            <label htmlFor="name" className="capitalize">
              name
            </label>
            <input
              {...register("name", {
                required: "name is required",
                minLength: { value: 3, message: "Minimum length 3" },
                maxLength: { value: 10, message: "Maximum length is 10" },
              })}
              type="text"
              name="name"
              id="name"
              placeholder="Enter name"
              className="border border-gray-800 rounded-md w-full p-2"
            />
            <p className="text-xs text-red-600">{errors.name?.message}</p>
          </div>
          <div className="mb-3 w-full flex flex-col items-start justify-center gap-2">
            <label htmlFor="email" className="capitalize">
              email
            </label>
            <input
              {...register("email", { pattern: /^[A-Za-z\.@]+$/i })}
              type="email"
              name="email" //
              id="email"
              placeholder="Enter email"
              className="border border-gray-800 rounded-md w-full p-2"
            />
            {errors?.email?.type === 'pattern' && (<p className="text-xs text-red-600">Invalid</p>)}
          </div>
          <div className="mb-3 w-full flex flex-col items-start justify-center gap-2">
            <label htmlFor="age" className="capitalize">
              age
            </label>
            <input
              {...register("age")}
              type="number"
              name="age"
              id="age"
              placeholder="Enter age"
              className="border border-gray-800 rounded-md w-full p-2"
            />
          </div>
          <div className="mb-3 w-full flex flex-col items-start justify-center gap-2">
            <label htmlFor="password" className="capitalize">
              password
            </label>
            <input
              {...register("password")} //
              type="password"
              name="password"
              id="password"
              placeholder="Enter password"
              className="border border-gray-800 rounded-md w-full p-2"
            />
          </div>
          <div className="mb-3 w-full flex flex-col items-start justify-center gap-2">
            <label htmlFor="confpassword" className="capitalize">
              confirm password
            </label>
            <input
              {...register("confpassword", {validate: data => {
                if(watch('password') !== data) {
                    return "password not match"
                }
                else {
                    return 'password Match'
                }
              }})}
              type="password"
              name="confpassword"
              id="confpassword"
              placeholder="Enter confirm password"
              className="border border-gray-800 rounded-md w-full p-2"
            />
            <p className="text-xs text-red-500">{errors.confpassword?.message}</p>
          </div>
          <div className="mb-3 w-full flex flex-col items-start justify-center gap-2">
            <label htmlFor="submit" className="capitalize"></label>
            <input
              type="submit"
              name="submit"
              id="submit"
              className="border border-gray-800 rounded-md py-2 px-4 bg-blue-600 text-white font-bold"
            />
          </div>
        </form>
      </div>
    </div>
  );
};

export default ReactHookForm;
